import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ClientLayoutComponent} from './Layout/client-layout/client-layout.component';
import {HomeComponent} from './Client/home/home.component';
import {LoginComponent} from './Client/login/login.component';
import {RegisterComponent} from './Client/register/register.component';
import {OffresComponent} from './Client/offres/offres.component';
import {CandidathomeComponent} from './candidat/candidathome/candidathome.component';
import {CandidatGuard} from './candidat.guard';
import {AdminGuard} from './admin.guard';
import {EmployeeGuard} from './employee.guard';
import {AdminrloginComponent} from './Admin/adminrlogin/adminrlogin.component';
import {EmployeeHomeComponent} from './Employee/employee-home/employee-home.component';
import {EmployeeLoginComponent} from './Employee/employee-login/employee-login.component';
import {AdminHomeComponent} from './Admin/admin-home/admin-home.component';
import {CandidatLayoutComponent} from './Layout/candidat-layout/candidat-layout.component';
import {AdminLayoutComponent} from './Layout/admin-layout/admin-layout.component';
import {EmployeeLayoutComponent} from './Layout/employee-layout/employee-layout.component';
import {AdminReseauxComponent} from './Admin/admin-reseaux/admin-reseaux.component';
import {PostulerComponent} from './candidat/postuler/postuler.component';
import {CandidatCandidatureComponent} from './candidat/candidat-candidature/candidat-candidature.component';
import {EmployeeCandidatureComponent} from './Employee/employee-candidature/employee-candidature.component';


const routes: Routes = [
  {
    path: '', component: ClientLayoutComponent,
    children: [
      {path: '', component: HomeComponent, pathMatch: 'full'},
      {path: 'login', component: LoginComponent},
      {path: 'register', component: RegisterComponent},
      {path: 'offres', component: OffresComponent}
    ]
  },
  {
    path: 'candidat', component: CandidatLayoutComponent, canActivate: [CandidatGuard],
    children: [
      {path: '', component: CandidathomeComponent, pathMatch: 'full'},
      {path: 'candidature', component: CandidatCandidatureComponent},
      {path: 'postuler/:id', component: PostulerComponent}
    ]
  },
  {
    path: 'Admin', component: AdminLayoutComponent,
    children: [
      {path: '', component: AdminHomeComponent, pathMatch: 'full',  canActivate: [AdminGuard]},
      {path: 'transactions', component: AdminReseauxComponent,  canActivate: [AdminGuard]},
      {path: 'login', component: AdminrloginComponent}
    ]
  },
  {
    path: 'Employee', component: EmployeeLayoutComponent,
    children: [
      {path: '', component: EmployeeHomeComponent, pathMatch: 'full',  canActivate: [EmployeeGuard]},
      {path: 'login', component: EmployeeLoginComponent},
      {path: 'candidatures', component: EmployeeCandidatureComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
