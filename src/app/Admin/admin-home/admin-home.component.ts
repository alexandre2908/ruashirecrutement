import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AllCallService} from '../../shared/all-call.service';
import {Employee} from '../../shared/Models/employee';
import {Candidat} from '../../shared/Models/candidat';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss']
})
export class AdminHomeComponent implements OnInit {
  formGroup: FormGroup;
  allEmployee: Employee[];
  message: '';

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private loginService: AllCallService) {
    this.formGroup = this.formBuilder.group({
      name: [null, Validators.required],
      fonction: [null, Validators.required],
      email: [null, Validators.compose([Validators.required, Validators.email])],
    });
  }

  ngOnInit() {
    this.loadAllEmployee();
  }
  createEmployee(post) {
    this.loginService.createEmployee(new Employee(0, post.name, post.email, post.fonction, ''))
      .subscribe(data => {
        // @ts-ignore
        if (data.response === 'success') {
          // @ts-ignore
          this.loadAllEmployee();
        } else {
          // @ts-ignore
          this.message = data.message;
        }
      });
  }

  loadAllEmployee() {
    this.loginService.listEmployee()
      .subscribe(data => {
        this.allEmployee = data;
        console.log(data);
      });
  }

  // tslint:disable-next-line:variable-name
  delete(id_employee: number) {
    this.loginService.deleteEmployee(id_employee)
      .subscribe(data => {
        // @ts-ignore
        if (data.response === 'success') {
          // @ts-ignore
          this.loadAllEmployee();
        } else {
          // @ts-ignore
          this.message = data.message;
        }
      });
  }
}
