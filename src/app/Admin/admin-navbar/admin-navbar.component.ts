import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-navbar',
  templateUrl: './admin-navbar.component.html',
  styleUrls: ['./admin-navbar.component.scss']
})
export class AdminNavbarComponent implements OnInit {
  user: any;

  constructor( private router: Router) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentAdmin'));
  }

  logout() {
    localStorage.removeItem('currentAdmin');
    this.router.navigate(['/Admin/login']);
  }
}
