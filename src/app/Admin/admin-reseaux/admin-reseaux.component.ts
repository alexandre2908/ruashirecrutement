import { Component, OnInit } from '@angular/core';
import {Offres} from '../../shared/Models/offres';
import {Transcations} from '../../shared/Models/Transcations';
import {AllCallService} from '../../shared/all-call.service';

@Component({
  selector: 'app-admin-reseaux',
  templateUrl: './admin-reseaux.component.html',
  styleUrls: ['./admin-reseaux.component.scss']
})
export class AdminReseauxComponent implements OnInit {
  allTransactions: Transcations[];
  constructor(private loginService: AllCallService) { }

  ngOnInit() {
    this.loadAllOffres();
  }
  loadAllOffres() {
    this.loginService.listTransaction()
      .subscribe(data => {
        this.allTransactions = data;
        console.log(data);
      });
  }
}
