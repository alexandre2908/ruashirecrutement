import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AllCallService} from '../../shared/all-call.service';

@Component({
  selector: 'app-adminrlogin',
  templateUrl: './adminrlogin.component.html',
  styleUrls: ['./adminrlogin.component.scss']
})
export class AdminrloginComponent implements OnInit {

  message: '';
  formGroup: FormGroup;
  post: any;
  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private loginService: AllCallService) {
    this.formGroup = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required, Validators.email])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  ngOnInit() {
  }

  LoginVerifyAdmin(post) {
    this.loginService.loginAdmin(post.email, post.password)
      .subscribe(data => {
        // @ts-ignore
        if (data.response === 'success') {
          // @ts-ignore
          localStorage.setItem('currentAdmin', JSON.stringify(data.all_data));
          this.router.navigate(['/Admin']);
        } else {
          // @ts-ignore
          this.message = data.message;
        }
      });
  }
}
