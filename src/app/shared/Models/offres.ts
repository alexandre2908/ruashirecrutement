export class Offres {
  id_offre: number;
  titre: string;
  description: string;
  date_fin: string;


  constructor(id_offre: number, titre: string, description: string, date_fin: string) {
    this.id_offre = id_offre;
    this.titre = titre;
    this.description = description;
    this.date_fin = date_fin;
  }
}
