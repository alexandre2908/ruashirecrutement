export class Candidat {
  // tslint:disable-next-line:variable-name
  id_candidat: number;
  name: string;
  email: string;
  // tslint:disable-next-line:variable-name
  numero_telephone: string;
  password: string;


  constructor(id_candidat: number, name: string, email: string, numero_telephone: string, password: string) {
    this.id_candidat = id_candidat;
    this.name = name;
    this.email = email;
    this.numero_telephone = numero_telephone;
    this.password = password;
  }
}
