export class Employee {
  id_employer: number;
  nom_complet: string;
  email: string;
  fonction: string;
  password: string;


  constructor(id_employer: number, nom_complet: string, email: string, fonction: string, password: string) {
    this.id_employer = id_employer;
    this.nom_complet = nom_complet;
    this.email = email;
    this.fonction = fonction;
    this.password = password;
  }
}
