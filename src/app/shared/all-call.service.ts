import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Candidat} from './Models/candidat';
import {Employee} from './Models/employee';
import {Offres} from './Models/offres';
import {Transcations} from './Models/Transcations';

@Injectable({
  providedIn: 'root'
})
export class AllCallService {

  // tslint:disable-next-line:variable-name
  private  url_server = 'http://127.0.0.1:5002/';
  // tslint:disable-next-line:variable-name
  constructor(private _request: HttpClient) { }

  loginVerifyCandidat(email: string, password: string) {
    return this._request.get(this.url_server.concat('candidat').concat('/login'), {params: {email, password}});
  }
  reigisterCandidat(candidat: Candidat) {
    return this._request.post(this.url_server.concat('candidat').concat('/register'), {candidat});
  }
  createEmployee(employee: Employee) {
    return this._request.post(this.url_server.concat('employee').concat('/create'), {employee});
  }
  createOffres(offres: Offres) {
    return this._request.post(this.url_server.concat('offres').concat('/create'), {offres});
  }

  listEmployee() {
    return this._request.get<Employee[]>(this.url_server.concat('employee').concat('/getAll'));
  }
  listOffres() {
    return this._request.get<Offres[]>(this.url_server.concat('offres').concat('/getAll'));
  }
  listTransaction() {
    return this._request.get<Transcations[]>(this.url_server.concat('admin').concat('/transaction'));
  }
  // tslint:disable-next-line:variable-name
  getOneOffres(id_offre) {
    return this._request.get<Offres>(this.url_server.concat('offres').concat('/getOne'), {params: {id_offre}});
  }
  getCandidatureCandidat(idCandidat) {
    return this._request.get(this.url_server.concat('candidature').concat('/getCandidat'), {params: {id_candidat: idCandidat}});
  }

  getCandidatures() {
    return this._request.get(this.url_server.concat('candidature').concat('/getAll'));
  }
  deleteEmployee(idemployee: number) {
    // @ts-ignore
    return this._request.get(this.url_server.concat('employee').concat('/delete'), {params: {id_employee: idemployee}});
  }
  // tslint:disable-next-line:variable-name
  deleteOffre(id_offre: number) {
      // @ts-ignore
    return this._request.get(this.url_server.concat('offres').concat('/delete'), {params: {id_offre}});
  }

  loginAdmin(email: string, password: string) {
    return this._request.get(this.url_server.concat('admin').concat('/login'), {params: {email, password}});
  }
  loginEmployee(email: string, password: string) {
    return this._request.get(this.url_server.concat('employee').concat('/login'), {params: {email, password}});
  }

  postuler(formData: FormData, headers: HttpHeaders) {
    return this._request.post<any>(this.url_server.concat('candidature').concat('/add'), formData, {headers});
  }
  validerCandidature(idEmployee, idCandidature) {
    // tslint:disable-next-line:max-line-length
    return this._request.post(this.url_server.concat('candidature').concat('/valider'), {id_employee: idEmployee, id_candidature: idCandidature});
  }

}
