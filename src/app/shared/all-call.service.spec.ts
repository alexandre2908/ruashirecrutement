import { TestBed } from '@angular/core/testing';

import { AllCallService } from './all-call.service';

describe('AllCallService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AllCallService = TestBed.get(AllCallService);
    expect(service).toBeTruthy();
  });
});
