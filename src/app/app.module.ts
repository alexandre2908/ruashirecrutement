import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientLayoutComponent } from './Layout/client-layout/client-layout.component';
import { HomeComponent } from './Client/home/home.component';
import { NavbarComponent } from './Client/navbar/navbar.component';
import { LoginComponent } from './Client/login/login.component';
import { RegisterComponent } from './Client/register/register.component';
import { OffresComponent } from './Client/offres/offres.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AllCallService} from './shared/all-call.service';
import {HttpClientModule} from '@angular/common/http';
import { CandidathomeComponent } from './candidat/candidathome/candidathome.component';
import { AdminHomeComponent } from './Admin/admin-home/admin-home.component';
import { EmployeeHomeComponent } from './Employee/employee-home/employee-home.component';
import { EmployeeLoginComponent } from './Employee/employee-login/employee-login.component';
import { AdminLayoutComponent } from './Layout/admin-layout/admin-layout.component';
import { EmployeeLayoutComponent } from './Layout/employee-layout/employee-layout.component';
import { CandidatLayoutComponent } from './Layout/candidat-layout/candidat-layout.component';
import { AdminNavbarComponent } from './Admin/admin-navbar/admin-navbar.component';
import { EmployeeNavbarComponent } from './Employee/employee-navbar/employee-navbar.component';
import { AdminReseauxComponent } from './Admin/admin-reseaux/admin-reseaux.component';
import { PostulerComponent } from './candidat/postuler/postuler.component';
import { CandidatCandidatureComponent } from './candidat/candidat-candidature/candidat-candidature.component';
import { AdminregisterComponent } from './Admin/adminregister/adminregister.component';
import { AdminrloginComponent } from './Admin/adminrlogin/adminrlogin.component';
import { EmployeeCandidatureComponent } from './Employee/employee-candidature/employee-candidature.component';
@NgModule({
  declarations: [
    AppComponent,
    ClientLayoutComponent,
    HomeComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    OffresComponent,
    CandidathomeComponent,
    AdminHomeComponent,
    EmployeeHomeComponent,
    EmployeeLoginComponent,
    AdminLayoutComponent,
    EmployeeLayoutComponent,
    CandidatLayoutComponent,
    AdminNavbarComponent,
    EmployeeNavbarComponent,
    AdminReseauxComponent,
    PostulerComponent,
    CandidatCandidatureComponent,
    AdminregisterComponent,
    AdminrloginComponent,
    EmployeeCandidatureComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [AllCallService],
  bootstrap: [AppComponent]
})
export class AppModule { }
