import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-candidathome',
  templateUrl: './candidathome.component.html',
  styleUrls: ['./candidathome.component.scss']
})
export class CandidathomeComponent implements OnInit {
  private user: any;

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentCandidat'));
  }

  isAuthentificated() {
    return !!localStorage.getItem('currentCandidat');
  }
}
