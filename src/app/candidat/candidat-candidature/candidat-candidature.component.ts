import { Component, OnInit } from '@angular/core';
import {AllCallService} from '../../shared/all-call.service';

@Component({
  selector: 'app-candidat-candidature',
  templateUrl: './candidat-candidature.component.html',
  styleUrls: ['./candidat-candidature.component.scss']
})
export class CandidatCandidatureComponent implements OnInit {

  user: any;
  allCandidature: any;
  constructor(private  apiCall: AllCallService) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentCandidat'));
    this.getAllCandidatureByCandidat(this.user.id_candidat);
  }

  getAllCandidatureByCandidat(ifCandidat) {
    this.apiCall.getCandidatureCandidat(ifCandidat).subscribe(data => {
      this.allCandidature = data;
      console.log(data);
    });
  }
}
