import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Offres} from '../../shared/Models/offres';
import {AllCallService} from '../../shared/all-call.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-postuler',
  templateUrl: './postuler.component.html',
  styleUrls: ['./postuler.component.scss']
})
export class PostulerComponent implements OnInit {
  offresId: string;
  candidatId: string;
  offre: Offres;
  submitted = false;
  message = '';
  filesToUpload: Array<File> = [];
  formGroup: FormGroup;

  user: any;
  constructor(private actRoute: ActivatedRoute,
              private loginService: AllCallService,
              private formBuilder: FormBuilder) {
    this.formGroup = this.formBuilder.group({
      files: [null, Validators.required]
    });
  }

  ngOnInit() {
    this.actRoute.paramMap.subscribe(params => {
      this.offresId = params.get('id');
    });
    this.getOneOffer(this.offresId);
    this.user = JSON.parse(localStorage.getItem('currentCandidat'));
    this.candidatId = this.user.id_candidat;
  }
  getOneOffer(idOffre) {
    this.loginService.getOneOffres(idOffre).subscribe(data => {
      this.offre = data;
      console.log(data);
    });
  }
  onFileMotivation(event) {
    this.filesToUpload = event.target.files as Array<File>;
  }

  postuler() {
    const formData: FormData = new FormData();
    const headers = new HttpHeaders();
    const files: Array<File> = this.filesToUpload;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0 ; i < files.length; i++) {
      formData.append('motivation[]', files[i], files[i].name);
    }
    headers.append('Content-Type', 'multipart/form-data');
    formData.append('id_offre', this.offresId);
    formData.append('id_candidat', this.candidatId);
    this.loginService.postuler(formData, headers).subscribe(data => {
      if (data.response === 'success') {
        this.submitted = true;
      } else {
        this.message = data.message;
      }
    });
  }
}
