import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(public router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.isAuthentificated()) {
      // not logged in so redirect to login page with the return url
      return true;
    }
    this.router.navigate(['Admin/login']);
    return false;
  }

  isAuthentificated() {
    if (localStorage.getItem('currentAdmin')) {
      return true;
    } else {
      return false;
    }
  }
}
