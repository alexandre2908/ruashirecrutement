import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeGuard implements CanActivate {

  constructor(public router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.isAuthentificated()) {
      // not logged in so redirect to login page with the return url
      return true;
    }
    this.router.navigate(['Employee/login']);
    return false;
  }

  isAuthentificated() {
    if (localStorage.getItem('currentEmployee')) {
      return true;
    } else {
      return false;
    }
  }
}
