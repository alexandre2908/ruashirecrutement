import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AllCallService} from '../../shared/all-call.service';
import {Candidat} from '../../shared/Models/candidat';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  formGroup: FormGroup;
  message: '';
  post: any;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private loginService: AllCallService) {
    this.formGroup = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required, Validators.email])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
      phone: [null, Validators.required],
      name: [null, Validators.required]
    });
  }

  ngOnInit() {
  }

  RegisterCandidat(post) {
    this.loginService.reigisterCandidat(new Candidat(0, post.name, post.email, post.phone, post.password))
      .subscribe(data => {
        // @ts-ignore
        if (data.response === 'success') {
          // @ts-ignore
          localStorage.setItem('currentCandidat', JSON.stringify(data.all_data));
          this.router.navigate(['/candidat']);
        } else {
          // @ts-ignore
          this.message = data.message;
        }
      });
  }

}
