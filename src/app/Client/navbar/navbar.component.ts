import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  user: any;

  constructor( private router: Router) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentCandidat'));
  }

  logout() {
    localStorage.removeItem('currentCandidat');
    this.router.navigate(['/login']);
  }

  isAuthentificated() {
    return !!localStorage.getItem('currentCandidat');
  }
}
