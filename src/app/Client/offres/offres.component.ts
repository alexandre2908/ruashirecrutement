import { Component, OnInit } from '@angular/core';
import {AllCallService} from '../../shared/all-call.service';
import {Offres} from '../../shared/Models/offres';

@Component({
  selector: 'app-offres',
  templateUrl: './offres.component.html',
  styleUrls: ['./offres.component.scss']
})
export class OffresComponent implements OnInit {
  allOffres: Offres[];

  constructor(private loginService: AllCallService) { }

  ngOnInit() {
    this.loadAllOffres();
  }

  loadAllOffres() {
    this.loginService.listOffres()
      .subscribe(data => {
        this.allOffres = data;
        console.log(data);
      });
  }

}
