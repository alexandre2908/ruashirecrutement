import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AllCallService} from '../../shared/all-call.service';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email = '';
  formGroup: FormGroup;
  post: any;
  password = '';
  message = '';
  alert_password = 'password is required and minimum length is 6';
  alert_username = 'username c\'ant be empty';

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private loginService: AllCallService) {
    this.formGroup = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required, Validators.email])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  ngOnInit() {
  }

  LoginVerify(post) {
    this.email = post.email;
    this.password = post.password;

    this.loginService.loginVerifyCandidat(this.email, this.password)
      .subscribe(data => {
       // @ts-ignore
        if (data.response === 'success') {
         // @ts-ignore
          localStorage.setItem('currentCandidat', JSON.stringify(data.all_data));
          this.router.navigate(['/candidat']);
       } else {
          // @ts-ignore
          this.message = data.message;
       }
      });
  }

}
