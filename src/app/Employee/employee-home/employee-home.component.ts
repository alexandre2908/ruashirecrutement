import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Employee} from '../../shared/Models/employee';
import {ActivatedRoute, Router} from '@angular/router';
import {AllCallService} from '../../shared/all-call.service';
import {Offres} from '../../shared/Models/offres';

@Component({
  selector: 'app-employee-home',
  templateUrl: './employee-home.component.html',
  styleUrls: ['./employee-home.component.scss']
})
export class EmployeeHomeComponent implements OnInit {

  formGroup: FormGroup;
  allOffres: Offres[];
  message: '';

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private loginService: AllCallService) {
    this.formGroup = this.formBuilder.group({
      title: [null, Validators.required],
      date_fin: [null, Validators.required],
      description: [null, Validators.required]
    });
  }
  ngOnInit() {
    this.loadAllOffres();
  }

  createOffres(post) {
    this.loginService.createOffres(new Offres(0, post.title, post.description, post.date_fin))
      .subscribe(data => {
        // @ts-ignore
        if (data.response === 'success') {
          // @ts-ignore
          this.loadAllOffres();
        } else {
          // @ts-ignore
          this.message = data.message;
        }
      });
  }

  loadAllOffres() {
    this.loginService.listOffres()
      .subscribe(data => {
        this.allOffres = data;
        console.log(data);
      });
  }

  delete(id_offre: number) {
    this.loginService.deleteOffre(id_offre)
      .subscribe(data => {
        // @ts-ignore
        if (data.response === 'success') {
          // @ts-ignore
          this.loadAllOffres();
        } else {
          // @ts-ignore
          this.message = data.message;
        }
      });
  }
}
