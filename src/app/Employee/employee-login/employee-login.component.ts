import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AllCallService} from '../../shared/all-call.service';

@Component({
  selector: 'app-employee-login',
  templateUrl: './employee-login.component.html',
  styleUrls: ['./employee-login.component.scss']
})
export class EmployeeLoginComponent implements OnInit {

  message: '';
  formGroup: FormGroup;
  post: any;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private loginService: AllCallService) {
    this.formGroup = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required, Validators.email])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  ngOnInit() {
  }
  LoginVerifyEmployee(post) {
    this.loginService.loginEmployee(post.email, post.password)
      .subscribe(data => {
        // @ts-ignore
        if (data.response === 'success') {
          // @ts-ignore
          localStorage.setItem('currentEmployee', JSON.stringify(data.all_data));
          this.router.navigate(['/Employee']);
        } else {
          // @ts-ignore
          this.message = data.message;
        }
      });
  }

}
