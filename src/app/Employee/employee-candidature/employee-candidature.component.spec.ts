import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeCandidatureComponent } from './employee-candidature.component';

describe('EmployeeCandidatureComponent', () => {
  let component: EmployeeCandidatureComponent;
  let fixture: ComponentFixture<EmployeeCandidatureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeCandidatureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeCandidatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
