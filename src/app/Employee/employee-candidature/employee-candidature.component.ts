import { Component, OnInit } from '@angular/core';
import {AllCallService} from '../../shared/all-call.service';

@Component({
  selector: 'app-employee-candidature',
  templateUrl: './employee-candidature.component.html',
  styleUrls: ['./employee-candidature.component.scss']
})
export class EmployeeCandidatureComponent implements OnInit {

  user: any;
  allCandidature: any;
  message: any;
  constructor(private  apiCall: AllCallService) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentEmployee'));
    this.getAllCandidature();
  }

  getAllCandidature() {
    this.apiCall.getCandidatures().subscribe(data => {
      this.allCandidature = data;
      console.log(data);
    });
  }

  valiserCandidature(idCandidature: any) {
    const id_employee = this.user.id_employee;
    this.apiCall.validerCandidature(id_employee, idCandidature).subscribe(data => {
      // @ts-ignore
      if (data.response === 'success') {
        // @ts-ignore
        this.message = data.message;
      }
    });
  }
}
